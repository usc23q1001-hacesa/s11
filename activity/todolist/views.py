from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem, Event
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, \
    RegisterForm, AddEventForm, UpdateProfileForm, ChangePasswordForm

# Create your views here.
def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
    event_list = Event.objects.filter(user_id = request.user.id)
    context = { 
        'todoitem_list': todoitem_list,
        'event_list': event_list,
        'user': request.user,
    }
    return render(request, 'todolist/index.html', context)

def todoitem(request, todoitem_id):
    object = get_object_or_404(ToDoItem, pk = todoitem_id)
    todoitem = model_to_dict(object)
    return render(request, 'todolist/todoitem.html', {**todoitem, 'user': request.user})

def register(request):
    template = 'todolist/register.html'
    
    context = {
        'is_registered': False,
        'is_successful': False,
        'password_different': False,
    }

    if request.method == 'GET':
        return render(request, template, context)
    
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if not form.is_valid():
            form = RegisterForm()
        else:
            context['username'] = form.cleaned_data['username']
            context['firstname'] = form.cleaned_data['firstname']
            context['lastname'] = form.cleaned_data['lastname']
            context['email'] = form.cleaned_data['email']
            context['password'] = form.cleaned_data['password']
            context['confirm_password'] = form.cleaned_data['confirm_password']

            # Check if password and confirm password match
            if context['password'] != context['confirm_password']:
                context['password_different'] = True
                return render(request, template, context)
            
            try:
                user = User.objects.get(username = context['username'])
            except User.DoesNotExist:
                user = None

            if user is not None:
                context['is_registered'] = True
                return render(request, template, context)

            user = User(
                    username = context['username'],
                    first_name = context['firstname'], 
                    last_name = context['lastname'],
                    email = context['email'],
                    is_staff =  False,
                    is_active = True
                )
            user.set_password(context['password'])
            user.save()
            context = { 'is_successful': True }
            return render(request, template, context)
    
def change_password(request):
    template = 'todolist/change_password.html'
    context = {
        'incorrect_password': False,
        'password_mismatch': False,
        'is_successful': False,
    }

    if request.method == 'POST':
        form = ChangePasswordForm(request.POST)
        if not form.is_valid():
            form = ChangePasswordForm()
        else:
            old_password = form.cleaned_data['old_password']
            new_password = form.cleaned_data['new_password']
            confirm_password = form.cleaned_data['confirm_password']
            context['old_password'] = old_password
            context['new_password'] = new_password
            context['confirm_password'] = confirm_password

            user = authenticate(username = request.user, password = old_password)
            if user is None:
                context['incorrect_password'] = True
            
            if user and new_password == confirm_password:
                context = { 'is_successful': True }
                user.set_password(new_password)
                user.save()
                login(request, user)
            else:
                context['password_mismatch'] = True
        
    return render(request, template, context)

def login_view(request):
    context = { 'error': False }
    if request.method == 'POST':
        
        form = LoginForm(request.POST)
        if not form.is_valid():
            form = LoginForm()
        else:
            credentials = {
                'username': form.cleaned_data['username'],
                'password': form.cleaned_data['password'],
            }

            user = authenticate(**credentials)
            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = { 'error': True }
    
    return render(request, 'todolist/login.html', context)

def logout_view(request):
    logout(request)
    return redirect('todolist:index')

def add_task(request):
    context = { 'error': False, 'user': request.user }
    if request.method == 'POST':

        form = AddTaskForm(request.POST)
        if not form.is_valid():
            form = AddTaskForm()
        else:
            task = {
                'task_name': form.cleaned_data['task_name'],
                'description': form.cleaned_data['description'],
                'date_created': timezone.now(),
            }
            
            duplicates = ToDoItem.objects.filter(task_name = task['task_name'])
            if not duplicates:
                ToDoItem.objects.create(**task, user_id = request.user.id)
                return redirect('todolist:index')
            else:
                context = { 'error': True }

    return render(request, 'todolist/add_task.html', context)

def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.get(pk = todoitem_id)
    context = { 
        'error': False, 'user': request.user,
        'todoitem_id': todoitem_id,
        'task_name': todoitem.task_name,
        'description': todoitem.description,
        'status': todoitem.status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)
        if not form.is_valid():
            form = UpdateTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem.task_name = task_name
                todoitem.description = description
                todoitem.status = status
                todoitem.save()
                return redirect('todolist:index')
            else:
                context = {
                    'error': True
                }

    return render(request, 'todolist/update_task.html', context)

def delete_task(request, todoitem_id):
    ToDoItem.objects.filter(pk = todoitem_id).delete()
    return redirect('todolist:index')

def add_event(request):
    context = { 'error': False, 'user': request.user }

    if request.method == 'POST':
        form = AddEventForm(request.POST)
        if not form.is_valid():
            print("invalid")
            form = AddEventForm()
        else:
            event = {
                'event_name': form.cleaned_data['event_name'],
                'description': form.cleaned_data['description'],
                'event_date': form.cleaned_data['event_date'],
            }

            context['event_name'] = event['event_name']
            context['description'] = event['description']
            
            duplicates = Event.objects.filter(event_name = event['event_name'])
            if not duplicates:
                Event.objects.create(**event, user_id = request.user.id)
                return redirect('todolist:index')
            else:
                context['error'] = True
    return render(request, 'todolist/add_event.html', context)

def event(request, id):
    object = get_object_or_404(Event, pk = id)
    event = model_to_dict(object)
    return render(request, 'todolist/event.html', {**event, 'user': request.user,})

def update_profile(request):
    context = { 
        'error': False, 
        'is_successful': False, 
        'user': request.user, 
        'first_name': request.user.first_name if not request.user.is_anonymous else '',
        'last_name': request.user.last_name if not request.user.is_anonymous else ''
    }
    
    if request.method == 'POST':
        form = UpdateProfileForm(request.POST)
        if not form.is_valid():
            form = UpdateProfileForm()
        else:
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            
            user = User.objects.get(pk = request.user.id)
            user.first_name = first_name
            user.last_name = last_name
            user.save()
            context['is_successful'] = True
            context['first_name'] = first_name
            context['last_name'] = last_name

    return render(request, 'todolist/update_profile.html', context)
