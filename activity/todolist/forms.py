from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length = 20)
    password = forms.CharField(label = 'Password', max_length = 20)

class AddTaskForm(forms.Form):
    task_name = forms.CharField(label = 'task_name', max_length = 50)
    description = forms.CharField(label = 'description', max_length = 200)

class UpdateTaskForm(forms.Form):
    task_name = forms.CharField(label = 'task_name', max_length = 50)
    description = forms.CharField(label = 'description', max_length = 200)
    status = forms.CharField(label = 'status', max_length = 50)

class RegisterForm(forms.Form):
    username = forms.CharField(label = 'username', max_length = 20)
    firstname = forms.CharField(label = 'firstname', max_length = 20)
    lastname = forms.CharField(label = 'lastname', max_length = 20)
    email = forms.CharField(label = 'email', max_length = 100)
    password = forms.CharField(label = 'password', max_length = 20)
    confirm_password = forms.CharField(label = 'confirm_password', max_length = 20)

class AddEventForm(forms.Form):
    event_name = forms.CharField(label = 'event_name', max_length = 50)
    description = forms.CharField(label = 'description', max_length = 200)
    event_date = forms.DateTimeField(label = 'event_date')

class UpdateProfileForm(forms.Form):
    first_name = forms.CharField(label = 'first_name', max_length = 20)
    last_name = forms.CharField(label = 'last_name', max_length = 20)

class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label = 'old_password', max_length = 20)
    new_password = forms.CharField(label = 'new_password', max_length = 20)
    confirm_password = forms.CharField(label = 'confirm_password', max_length = 20)